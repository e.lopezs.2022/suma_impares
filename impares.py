def suma_impares_entre_numeros():
    num1 = int(input("Dame un número entero no negativo: "))

    while num1 < 0:
        print("Por favor, introduce un número entero no negativo.")
        num1 = int(input("Dame un número entero no negativo: "))

    num2 = int(input("Dame otro número entero no negativo: "))

    while num2 < 0:
        print("Por favor, introduce un número entero no negativo.")
        num2 = int(input("Dame otro número entero no negativo: "))

    if num1 > num2:
        num1, num2 = num2, num1

    suma_impares = sum(i for i in range(num1, num2 + 1) if i % 2 != 0 or i == 2)

    print(f"La suma de los números impares entre {num1} y {num2} es: {suma_impares}")

suma_impares_entre_numeros()